import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   private long numerator;
   private long denominator;

   /** Main method. Different tests. */
   public static void main (String[] param) {
      new Lfraction(10, 20);
   }

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) {
         throw new RuntimeException("Can't create a fraction with 0 as the denominator");
      }

      numerator = a;
      denominator = b;

      if (denominator < 0) {
         numerator = -numerator;
         denominator = -denominator;
      }

      long gcd = Math.abs(getGCD(a, b));
      numerator /= gcd;
      denominator /= gcd;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return getNumerator() + "/" + getDenominator();
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (!(m instanceof Lfraction)) {
         return false;
      }

      Lfraction otherFraction = (Lfraction) m;

      return compareTo(otherFraction) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(getNumerator(), getDenominator());
   }

   // https://www.baeldung.com/java-greatest-common-divisor#euclid's%20algorithm
   private static long getGCD(long a, long b) {
      if (b == 0) {
         return a;
      }

      return getGCD(b, a % b);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long gcm = getDenominator() * m.getDenominator();
      long a = getNumerator() * m.getDenominator();
      long am = m.getNumerator() * getDenominator();
      long numerator = a + am;
      long gcd = getGCD(numerator, gcm);

      return new Lfraction(numerator / gcd, gcm / gcd);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long numerator = getNumerator() * m.getNumerator();
      long denominator = getDenominator() * m.getDenominator();

      return new Lfraction(numerator, denominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (getNumerator() == 0) {
         throw new RuntimeException("Can't inverse " + this + " because denominator would be 0");
      }
      return new Lfraction(getDenominator(), getNumerator());
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-getNumerator(), getDenominator());
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return this.plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      long numerator = getNumerator() * m.getDenominator();
      long denominator = getDenominator() * m.getNumerator();

      if (denominator == 0) {
         throw new RuntimeException("Can't divide by 0 at: " + this + " / " + m);
      }

      return new Lfraction(numerator, denominator);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction diff = minus(m);

      if (diff.getNumerator() < 0) {
         return -1;
      } else if (diff.getNumerator() == 0) {
         return 0;
      } else {
         return 1;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(getNumerator(), getDenominator());
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (getNumerator() - (getNumerator() % getDenominator())) / getDenominator();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long a = getNumerator() % getDenominator();
      long b = getDenominator();

      if (a == 0L) {
         b =  1L;
      }

      return new Lfraction(a, b);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) getNumerator() / getDenominator();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (!s.contains("/")) {
         throw new RuntimeException("Invalid syntax at " + s + ".\nExample: 1/2");
      }

      String[] numbers = s.split("/");

      if (numbers.length != 2) {
         throw new RuntimeException("Invalid syntax at " + s + ", missing numerator or denominator.\nExample: 1/2");
      }

      try {
         long nominator = Long.parseLong(numbers[0]);
         long denominator = Long.parseLong(numbers[1]);

         return new Lfraction(nominator, denominator);
      } catch (NumberFormatException e) {
         throw new RuntimeException("Failed to convert nominator or denominator at " + s + "\nExample: 1/2");
      }
   }

   public Lfraction pow(int modifier) {
      if (modifier == 0) {
         return new Lfraction(1, 1);
      } else if (modifier == -1) {
         return this.inverse();
      } else if (modifier > 1) {
         return this.times(this.pow(modifier - 1));
      } else if (modifier < 0) {
         return this.pow(-modifier).inverse();
      }

      return new Lfraction(numerator, denominator);
   }
}

